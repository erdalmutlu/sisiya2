module Sequel
  class Dataset
    def schema()
      schema = []
      self.db.schema(self).each{|colid, coldef|
        next unless self.columns.include?(colid)
        schema << [colid, coldef]
      }
      schema
    end

    def columntype(colname)
      self.schema.each{|colid, coldef|
        next unless colid == colname
        return coldef[:type]
      }
      raise ArgumentError, "#{colname} not part of #{self.inspect}"
    end
  end
end
