require 'pathname'
require 'bundler/setup'
require 'yaml'
require_relative 'sequel_mod'
Bundler.require(:default)
APP_ROOT = Pathname.new(File.expand_path('../../', __FILE__))
APP_NAME = File.basename(APP_ROOT)
ENVIRONMENT = :development
DB = Sequel.connect(YAML.load(File.read(File.join(APP_ROOT,'config','database.yml')))[ENVIRONMENT.to_s])