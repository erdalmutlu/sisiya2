
	    $(function() {

        $("#jsGrid").jsGrid({
            height: "600px",
            width: "100%",
     
            filtering: true,
            editing: false,
            sorting: true,
            inserting: false,
            paging: true,
            autoload: true,
     
            pageSize: 15,
            pageButtonCount: 5,
            rowClick: function(args) {window.location.href = '/users/'+args["item"]["id"]+'/edit';},
            deleteConfirm: "${i18nhelper("l000000110")!""}?",
     
            controller: 

            {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/users",
                        data: filter,
                        error: function(resp){
                        	alert("error"+resp);
                        }
                    });
                },
                
                insertItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/users",
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                updateItem: function(item) {
                    return $.ajax({
                        type: "PUT",
                        url: "/users/"+item.id,
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/users/"+item.label,
                        dataType: "json",
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
            },
     
            fields: [
                { name: "id", type: "number", width: 50 },
                { name: "username",title: "${i18nhelper("l000000244")!""}", type: "text",width: 200},
                { name: "email",title: "${i18nhelper("l000000332")!""}", type: "text",width: 200},
                { type: "control" }
            ]
        });

    });
