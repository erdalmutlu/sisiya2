
	    $(function() {

        $("#jsGrid").jsGrid({
            height: "600px",
            width: "100%",
     
            filtering: true,
            editing: true,
            sorting: true,
            inserting: false,
            paging: true,
            autoload: true,
     
            pageSize: 15,
            pageButtonCount: 5,
     
            deleteConfirm: "${i18nhelper("l000000110")!""}?",
     
            controller: 

            {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/labels",
                        data: filter,
                        error: function(resp){
                        	alert("error"+resp);
                        }
                    });
                },
                
                insertItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/labels",
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                updateItem: function(item) {
                    return $.ajax({
                        type: "PUT",
                        url: "/labels/"+item.id,
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/labels/"+item.label,
                        dataType: "json",
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                    return $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/labels",
                        data: filter,
                        error: function(resp){
                        	alert("error"+resp);
                        }
                    });
                },
            },
     
            fields: [
                { name: "language_id",title: "${i18nhelper("l000000083")!""}", type: "select",items: [{id:1,name: "English"},{id:2,name: "Türkçe"}] ,valueField: "id", textField: "name", width: 50 },
                { name: "label",title: "${i18nhelper("l000000107")!""}", type: "text", width: 100 },
                { name: "str",title: "${i18nhelper("l000000196")!""}", type: "text", width: 200 },
                { type: "control" }
            ]
        });

        $("#jsGrid").jsGrid("search", { label: "${newlabel!}" }).done(function() {
            console.log("filtering completed ${newlabel!}");
        });
    });
