
	    $(function() {

        $("#jsGrid").jsGrid({
            height: "600px",
            width: "100%",
     
            filtering: true,
            editing: false,
            sorting: true,
            inserting: false,
            paging: true,
            autoload: true,
     
            pageSize: 15,
            pageButtonCount: 5,
     
            deleteConfirm: "${i18nhelper("l000000110")!""}?",
     
            controller: 

            {
                loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "/system_types",
                        data: filter,
                        error: function(resp){
                        	alert("error"+resp);
                        }
                    });
                },
                
                insertItem: function(item) {
                    return $.ajax({
                        type: "POST",
                        url: "/system_types/create",
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                updateItem: function(item) {
                    return $.ajax({
                        type: "PUT",
                        url: "/system_types/"+item.id,
                        dataType: "json",
                        data: item,
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
                
                deleteItem: function(item) {
                    return $.ajax({
                        type: "DELETE",
                        url: "/system_types/delete/"+item.label,
                        dataType: "json",
                        error: function(resp){
                        	alert(resp.responseJSON.err);
                        }
                    });
                },
            },
     
            fields: [
                { name: "id", type: "number", width: 50 },
                { name: "str",title: "${i18nhelper("l000000196")!""}", type: "text",width: 200},
                
                { type: "control" }
            ]
        });

    });
