$(function() {
  var app_rej_message = "";
  var upload = function(input,model,model_id) {
	  var bck =  $('#profile-picture').attr("src");
	  $('#profile-picture').attr("src","/img/loading.gif");
    if (input.files && input.files[0]) {
      $(input.files).each(function(file) {
        if(input.files[file].size>10485760){
          alert("Please select files less then 5mb");
          return true;
        }
        var reader = new FileReader();
        var file_object = new Object();
        reader.readAsDataURL(this);
        reader.onload = function(e) {
          console.log(input.files[file]);
          file_object.name = input.files[file].name;
          file_object.type = input.files[file].type;
          file_object.size = input.files[file].size;
          file_object.model = model;
          file_object.model_id = model_id;
          file_object.data = e.target.result;
          //console.log(file_object);
          $.ajax({
            contentType: 'application/json',
            data: JSON.stringify(file_object),
            dataType: 'json',
            success: function(data){
              if(data.success){
            	//refresh image
            	$('#profile-picture').attr("src",file_object.data);
            	$('#profile-picture').fadeTo('slow', 0.5).fadeTo('slow', 1.0);
                //$('#files').append(fileHtml(file_object.name,data.id));
            	  //alert("success");
            	var user_object = new Object();
            	user_object.avatar = file_object.name;
            	user_object.id = ${(current_user.id)!""}
            	$.ajax({
            		contentType: 'application/json',
            		data: JSON.stringify(user_object),
            		dataType: 'json',
            		error: function(){
            			console.log("user update failed");
                    },
                	type: 'PUT',
                	url: '/users/${(current_user.id)!""}'
            	});
              }else{
            	  console.log(data);
                  alert(data.message);
                  $('#profile-picture').attr("src",bck);
              }
            },
            error: function(){
            	console.log("failed")
            },
            xhr: function() {
              var xhr = new window.XMLHttpRequest();
              xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                  var percentComplete = evt.loaded / evt.total;
                  //Do something with upload progress here
                  console.log(percentComplete*100+" %");
                  //$('#upload-progress-bar').css("width",percentComplete*100+"%");
                }
              }, false);
              return xhr;
            },
            processData: false,
            type: 'POST',
            url: '/uploads'
          });
        }
      });
    }
  }
  $(":file").filestyle({input: false,badge: false});
  $('#avatar-upload').on('change',function(){
	  //$('#upload-progress').show();
	  //$('#upload-progress-bar').css("width",0+"%");
	  upload($('#avatar-upload')[0],"User",$(':file').data("id"));
  });
});