//Auto generated file
//Do not edit this file
package org.sisiya.ui.standart;
import org.sql2o.Connection;
import org.sql2o.Query;
import java.util.List;
import org.sisiya.ui.Application;

				
public class Label extends Model{

	//Fields  
  protected int id;
  protected int language_id;
  protected String label;
  protected String str;

	private	ModelHooks mh;
	private Boolean isNew;
 //Constructer
  public Label(){
  	isNew = true;
  	//Default Constructer
    fields.add(new Field("id","int","label-id",true,true));
   fields.add(new Field("language_id","int","label-language_id",false,false));
   fields.add(new Field("label","String","label-label",false,false));
   fields.add(new Field("str","String","label-str",false,false));
  }
  public void registerHooks(ModelHooks _mh){
	  mh = _mh;
  }
 //Setters

  public void setId(int _id){
    id = _id;
  }
  public void setLanguage_id(int _language_id){
    language_id = _language_id;
  }
  public void setLabel(String _label){
    label = _label;
  }
  public void setStr(String _str){
    str = _str;
  }
  public void setIsNew(boolean b){
	isNew = b;    
  }
  //Getters

  public int getId(){
    return id;
  }
  public int getLanguage_id(){
    return language_id;
  }
  public String getLabel(){
    return label;
  }
  public String getStr(){
    return str;
  }
	public Boolean isNew() {
		return isNew;
  }
	//Data Access Methods
	//Data Access Methods
	public boolean save(Connection _connection){
		return(save(_connection,true,true));
	}
	public boolean save(Connection _connection,boolean doValidate,boolean executeHooks){
		if(doValidate){
			try {
				if(!mh.validate(_connection)){
					return(false);
				}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			if(!isReadyToSave()){
				return(false);
			}
		}
		if(executeHooks){
			try {
				if(isNew()){
					if(!mh.beforeInsert(_connection)){return(false);}
				}else{
					if(!mh.beforeUpdate(_connection)){return(false);}
				}
				if(!mh.beforeSave(_connection)){return(false);}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			//Actual db update
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				  	if(!mh.afterInsert(_connection)){return(false);}
				  	if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
					if(!mh.afterUpdate(_connection)){return(false);}
					if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}else{
			//Actual db operation without hooks
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}
		return true;
	}
	public boolean destroy(Connection _connection,boolean executeHooks){
		if(executeHooks){
			if(!mh.beforeDestroy(_connection)){return(false);}
		}
		if(!delete(_connection)){return(false);}
		if(executeHooks){
			if(!mh.afterDestroy(_connection)){return(false);}
		}
		return(true);
	}
	public boolean destroy(Connection _connection){
		return(destroy(_connection,true));
	}

	//Private Data Acess utility Methods
	private boolean insert(Connection _connection){
    Query query;
    query = _connection.createQuery(insertSql(),true);
	
	
			query = query.addParameter("language_idP",language_id);
	query = query.addParameter("labelP",label);
	query = query.addParameter("strP",str);

		
    id = (int) query.executeUpdate().getKey();
    
    return(true);
	}
	private boolean update(Connection _connection){
    Query query;
    query = _connection.createQuery(updateSql());
	
	query = query.addParameter("idP",id);
	
	query = query.addParameter("language_idP",language_id);
	
	query = query.addParameter("labelP",label);
	
	query = query.addParameter("strP",str);

    query.executeUpdate();
    return(true);
	}
	private boolean delete(Connection _connection){
		Query query;
		query = _connection.createQuery(deleteSql());
    query.addParameter("idP",id);
    query.executeUpdate();
    return(true);
	}
  private String insertSql(){
    String  querySql = "insert into labels( ";
      querySql += "language_id,";
      querySql += "label,";
      querySql += "str)";
  	
    querySql += "values (";
      querySql += ":language_idP,";
      querySql += ":labelP,";
      querySql += ":strP)"; 
  	
    return querySql;
  }
  private String updateSql(){
    String  querySql = "update labels set ";
      querySql += "language_id = :language_idP, " ;
      querySql += "label = :labelP, " ;
      querySql += "str = :strP ";
  	
			querySql += " where id = :idP";
	
	
	
	
	
    
   return querySql;
  }
  private String deleteSql(){
			String querySql = "delete from labels where id = :idP";
	
	
	
	
	
  	
  	return querySql;
  }

}
