package org.sisiya.ui.controllers;

import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.Constants;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.FlashHelper;
import org.sisiya.ui.helpers.I18nHelper;
import org.sisiya.ui.models.*;
import org.sql2o.Connection;

import com.google.gson.Gson;

import ca.krasnay.sqlbuilder.SelectBuilder;

public class LabelsController extends ApplicationController implements CrudController{

	public LabelsController(String _path) {
		super(_path);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String indexAction(Request req, Response res) {
		if(respondToJson(req)){
			List<Label> labels;
			Gson gson = new Gson();
			Label label = new org.sisiya.ui.models.Label();
			SelectBuilder sb = new SelectBuilder().column("*").from("labels");
			req.queryParams().forEach((p)->{
				if(req.queryParams(p)!=null && !req.queryParams(p).isEmpty()){
					if(label.getField(p).getType()=="String"){
						sb.where(p+" LIKE "+"'%"+req.queryParams(p)+"%'");
					}else{
						sb.where(p+" = "+req.queryParams(p));
					}
				}
			});
			Connection conn = Application.db.open();
			labels = conn.createQuery(sb.toString()).executeAndFetch(Label.class);
			conn.close();
			String json = gson.toJson(labels);
			res.body(json);
			return("");			
		}
		if(respondToHtml(req)){
			model = getDefaultModel(req.session());
			Map<Object, Object> customjsmodel = new HashMap<>();
			customjsmodel.put("i18nhelper", new I18nHelper());
			model.put("customjs", ApplicationHelper.parseFreemarkerAsset("assets/labels.js",customjsmodel));
			return ApplicationHelper.render(model, viewPath("index"));			
		}
		return "";
	}

	@Override
	public String newAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createAction(Request req, Response res) {
		if(respondToHtml(req)){
			Connection c = Application.db.open();
			Label label = new Label();
			int i=0;
			try {
				i = Label.getLastLabelNumber(c);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			c.close();
			final int j = i;
				Language.all().forEach((lang)->{
				label.setLanguage_id(lang.getId());
				label.setStr(ApplicationHelper.getParams(req.body()).get(lang.getId()+"_str"));
				label.setLabel("ul"+String.format("%08d", j+1));
				Connection ct = Application.db.beginTransaction();
				if(label.save(ct)){
					ct.commit();
					ct.close();
					req.session().attribute("label",label.getLabel());
					FlashHelper.success(req.session(), "Label "+("ul"+String.format("%08d", j+1))+" created successsfully");
				}else{
					ct.rollback();
					ct.close();
					if(!label.getErrors().isEmpty()){
						FlashHelper.error(req.session(), label.getErrors().get(0).getErr());
					}
				}
			});
			Application.i18n.loadTranslationsFromDb(Application.db.open());
			res.redirect("/labels");
			return("");			
		}
		return "";
	}

	@Override
	public String showAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String editAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAction(Request req, Response res) {
		if(respondToHtml(req)){
			Label label = null;
			HashMap<String, Object> qparams = new HashMap<String,Object>();
			qparams.put("idP",Integer.parseInt(req.params(":id")));
			label = (Label) Label.where("id = :idP",qparams).get(0);
			label.setIsNew(false);
			Connection c = Application.db.beginTransaction();
			Map<String, String> params = ApplicationHelper.getParams(req.body());
			label.setStr(params.get("str").toString());
			//label.setLanguage_id(Integer.parseInt((String) params.get("language_id")));
			//label.setLabel((String)params.get("label"));
			if(label.save(c)){
				c.commit();
				c.close();
				res.body("{\"success\": true}");
			}else{
				c.rollback();
				c.close();
				Gson gson = new Gson();
				if(!label.getErrors().isEmpty()){
					res.body(gson.toJson(label.getErrors().get(0)));
				}
				res.status(500);	//Error				
			}
			Application.i18n.loadTranslationsFromDb(Application.db.open());
			return "";			
		}
		return "";
	}

	@Override
	public String deleteAction(Request req, Response res) {
		if(respondToJson(req)){
			Connection connection = Application.db.open();
			Gson gson = new Gson();
			connection.createQuery("select * from labels where label = :id").addParameter("id", ""+req.params(":id")).executeAndFetch(Label.class).forEach((label)->{
				Connection c = Application.db.beginTransaction();
				if(label.destroy(c)){
					c.commit();
					c.close();
					res.body("{\"success\": true}");
				}else{
					c.rollback();
					c.close();
					if(!label.getErrors().isEmpty()){
						res.body(gson.toJson(label.getErrors().get(0)));
					}
					res.status(500);	//Error				
				}	
			});
			Application.i18n.loadTranslationsFromDb(connection);
			connection.close();
			return "";				
		}
		return "";
	}

}
