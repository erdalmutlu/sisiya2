package org.sisiya.ui.lib;

import java.util.HashMap;

import org.sisiya.ui.Constants;
import org.sisiya.ui.controllers.CrudController;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.delete;
import static spark.Spark.put;

public abstract class Router {
	public HashMap<String, CrudController> routes;
	
	public Router(){
		routes = new HashMap<>();
	}
	
	public String newPath(String controller){
		return buildPath(controller, "new");
	}
	public String indexPath(String controller){
		return buildPath(controller, "");
	}
	public String createPath(String controller){
		return buildPath(controller, "");
	}
	public String showPath(String controller){
		return buildPath(controller, ":id");
	}
	public String editPath(String controller){
		return buildPath(controller, ":id/edit");
	}
	public String updatePath(String controller){
		return buildPath(controller, ":id");
	}
	public String deletePath(String controller){
		return buildPath(controller, ":id");
	}
	public String buildPath(String controller,String path){
		StringBuilder pathBuilder = new StringBuilder();
		pathBuilder.append("/");
		pathBuilder.append(controller);
		if(!path.isEmpty()){
			pathBuilder.append("/");
			pathBuilder.append(path);
		}
		return(pathBuilder.toString());
	}
	// Standart Routes
	public void initStandartRoutes() {
		routes.forEach((k,v)->{
			//index path
			get(indexPath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.indexAction(req, res);
			});
			get(indexPath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.indexAction(req, res);
			});
			//new path
			get(newPath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.newAction(req, res);
			});
			get(newPath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.newAction(req, res);
			});
			//create path
			post(createPath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.createAction(req, res);
			});
			post(createPath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.createAction(req, res);
			});
			//show path
			get(showPath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.showAction(req, res);
			});
			get(showPath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.showAction(req, res);
			});
			//edit path
			get(editPath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.editAction(req, res);
			});
			get(editPath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.editAction(req, res);
			});
			//update path
			put(updatePath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.updateAction(req, res);
			});
			put(updatePath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.updateAction(req, res);
			});
			//delete path
			delete(deletePath(k),Constants.RESPOND_TO_JSON,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_JSON);
				return v.deleteAction(req, res);
			});
			delete(deletePath(k),Constants.RESPOND_TO_HTML,(req,res) -> {
				req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
				return v.deleteAction(req, res);
			});
		});
	}
	
}
