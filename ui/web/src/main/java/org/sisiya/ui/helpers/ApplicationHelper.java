package org.sisiya.ui.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;
import org.sisiya.ui.models.User;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

import spark.ModelAndView;
import spark.Session;
import spark.template.freemarker.FreeMarkerEngine;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class ApplicationHelper {
	public static String render(Map<String, Object> model, String templatePath) {
		if(model.get("layout")==null){
			model.put("layout", "/layouts/layout.vtl");	//Default Layout
		}else{
			String	layout;
			layout = (String) model.get("layout");
			model.remove("layout");
			model.put("layout", "/layouts/"+layout+".vtl");
		}
	    return new FreeMarkerEngine().render(new ModelAndView(model, templatePath+".vtl"));
	}
	public static Map<String, String> getParams(String body){
		Map<String, String> params = new LinkedHashMap<String, String>();
		String[] pairs = body.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			try {
				params.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return(params);
			}
		}
		return(params);
	}
	public static void setCurrentUser(Session session,User user){
		session.attribute("user_id",user.getId());
		user.setPassword_hash("*****");
		session.attribute("current_user",user);
	}
	public static JsonArray resultSetToJson(List<Map<String, Object>> rs){
		JsonArray rows = new JsonArray();
		rs.forEach((s)->{
			JsonObject o = new JsonObject();
			((Map<String, Object>) s).keySet().forEach((k)->{
				o.addProperty(k, ""+((Map<String, Object>) s).get(k));
			});
			rows.add(o);
		});
		return rows;
	}
	public static void addToSessionHistory(Session session,String path){
		BackToQueue queue;
		if(session.attribute("back_to")==null){
			session.attribute("back_to",new BackToQueue(50));
			queue = session.attribute("back_to");
		}else{
			queue = session.attribute("back_to");
		}
		queue.add(path);
	}
	public static String getPreviousPath(Session session,String current_path){
		BackToQueue queue;
		if(session.attribute("back_to")==null){
			return "/";	//Return root path
		}else{
			queue = session.attribute("back_to");
		}
		return queue.getPreviousPath(current_path);
	}
	public static String getStatusImageUrl(int status_id) {
		String prefix ="/img/sisiya/";
		String suffix = "_big.png";
		if(status_id == 1){
			return(prefix+"Info"+suffix);
		}
		else if(status_id > 1 && status_id < 4)
			return(prefix+"Ok"+suffix);
		else if(status_id >= 4  && status_id < 8)
			return(prefix+"Warning"+suffix);
		else if(status_id >= 8 && status_id < 16)
			return(prefix+"Error"+suffix);
		else
			return("/img/sisiya/Unavailable_big.png");
	}
	@SuppressWarnings({ "deprecation" })
	public static String parseFreemarkerAsset(String asset,Map variableMap){
	    Template template = null;
	    JavaScriptCompressor jsc = null;
		try {
			template = new Template(null, new BufferedReader(new InputStreamReader(ApplicationHelper.class.getClassLoader().getResourceAsStream(asset))));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return asset+" not found";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return asset+" not found";
		}
	 
	    final Writer writer = new StringWriter();
	 
	    try {
			template.process(variableMap, writer);
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return asset+" failed to parse";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return asset+" failed to parse";
		}
	    Reader reader = new BufferedReader(new StringReader(writer.toString()));
	    try {
				jsc = new JavaScriptCompressor(reader, new ErrorReporter() {
				
				@Override
				public void warning(String arg0, String arg1, int arg2, String arg3, int arg4) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public EvaluatorException runtimeError(String arg0, String arg1, int arg2, String arg3, int arg4) {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public void error(String arg0, String arg1, int arg2, String arg3, int arg4) {
					// TODO Auto-generated method stub
					
				}
			});
		} catch (EvaluatorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Writer copmressedWriter = new StringWriter();
	    try {
			jsc.compress(copmressedWriter, 100, true, false, false, false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return copmressedWriter.toString();
	}
	public static boolean loggedIn(Session session){
		return(session.attribute("user_id")!=null);
	}
}
