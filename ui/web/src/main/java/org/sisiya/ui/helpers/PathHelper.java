package org.sisiya.ui.helpers;

import java.util.List;

import org.sisiya.ui.Application;

import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class PathHelper implements TemplateMethodModelEx{

	@Override
	public SimpleScalar exec(List args) throws TemplateModelException {
		if(args.size()!=2){
			return(SimpleScalar.newInstanceOrNull("/pathNotFound"));
		}
		String path = Application.router.buildPath(""+args.get(0), ""+args.get(1));
		return(SimpleScalar.newInstanceOrNull(path));	
	}
}
