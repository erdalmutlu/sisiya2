package org.sisiya.ui.models;

import java.util.List;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.standart.Error;
import org.sisiya.ui.standart.ModelHooks;
import org.sql2o.Connection;
import org.sql2o.Query;

public class Label extends org.sisiya.ui.standart.Label implements ModelHooks{
	
	public Label() {
		// TODO Auto-generated constructor stub
		registerHooks(this);
	}
	@Override
	public boolean beforeSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeInsert(Connection _connection) {

		return true;
	}

	@Override
	public boolean afterInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeDestroy(Connection _connection) {
		if(label.startsWith("l")){
			this.errors.add(new Error("","could not delete standart label"));
			return false;
		}
		return true;
	}

	@Override
	public boolean afterDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean validate(Connection _connection) {
		if(label.startsWith("l")){
			this.errors.add(new Error("","label should not start with l"));
			return false;
		}
		if(label.isEmpty()){
			this.errors.add(new Error("","label should not be empty"));
			return false;			
		}
		if(str.isEmpty()){
			this.errors.add(new Error("","label description should not be empty"));
			return false;			
		}
		return true;
	}
	//Static methods
	public static int getLastLabelNumber(Connection c){	
		int i;
		List<Label> list= c.createQuery("select * from labels where label like :param and language_id=:lid order by label desc").addParameter("param", "ul%").addParameter("lid", 1).executeAndFetch(Label.class);
		if(list.size()>0){
			i = Integer.parseInt(list.get(0).getLabel().replaceAll("\\D+",""));			
		}else{
			i=0;
		}
		c.close();
		return(i);
	}
	public static List<Label> all(){
		Connection c = Application.db.open();
		List<Label> objects = c.createQuery("select * from labels").executeAndFetch(Label.class);
		c.close();
		return objects;
	}
	public static List<Label> where(String query,Map<String, Object> params){
		Connection c = Application.db.open();
		List<Label> objects = null;
		Query q = c.createQuery("select * from labels where "+query);
		params.forEach((k,v)->{
			q.addParameter(k, v);
		});
		objects = q.executeAndFetch(Label.class);
		return objects;
	}
}
