package org.sisiya.ui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sql2o.Connection;

import spark.Request;
import spark.Response;

public class SystemStatusController  extends ApplicationController implements CrudController{

	public SystemStatusController(String _path) {
		super(_path);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String indexAction(Request req, Response res) {
		List<?> row;
		List<Map> list = new ArrayList<>();
		model = getDefaultModel(req.session());
		String query = "select 	b.id,b.hostname,a.status_id from 	system_status a,systems b where	a.system_id = b.id and	b.enabled = true";
		try (Connection conn = Application.db.open()){
			 row = conn.createQuery(query).executeAndFetchTable().asList();
			 for (int i = 0; i < row.size(); i++) {
				 Map m = ((Map)row.get(i));
				 Map<String, String> temp = new HashMap<>();
				 temp.put("id",""+m.get("id"));
				 temp.put("hostname",(String) m.get("hostname"));
				 temp.put("status_id",""+m.get("status_id"));
				 temp.put("img_url", ApplicationHelper.getStatusImageUrl((int) m.get("status_id")));
				 list.add(temp);
			 }
			model.put("systemStatusList", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ApplicationHelper.render(model, viewPath("index"));
	}

	@Override
	public String newAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String showAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String editAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

}
