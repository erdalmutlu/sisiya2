//Auto generated file
//Do not edit this file
package org.sisiya.ui.standart;
import org.sql2o.Connection;
import org.sql2o.Query;
import java.util.List;
import org.sisiya.ui.Application;

				import java.util.Date;
	
public class System_status extends Model{

	//Fields  
  protected int id;
  protected int system_id;
  protected int status_id;
  protected Date update_time;
  protected Date change_time;

	private	ModelHooks mh;
	private Boolean isNew;
 //Constructer
  public System_status(){
  	isNew = true;
  	//Default Constructer
    fields.add(new Field("id","int","system_status-id",true,true));
   fields.add(new Field("system_id","int","system_status-system_id",false,false));
   fields.add(new Field("status_id","int","system_status-status_id",false,false));
   fields.add(new Field("update_time","Date","system_status-update_time",false,false));
   fields.add(new Field("change_time","Date","system_status-change_time",false,false));
  }
  public void registerHooks(ModelHooks _mh){
	  mh = _mh;
  }
 //Setters

  public void setId(int _id){
    id = _id;
  }
  public void setSystem_id(int _system_id){
    system_id = _system_id;
  }
  public void setStatus_id(int _status_id){
    status_id = _status_id;
  }
  public void setUpdate_time(Date _update_time){
    update_time = _update_time;
  }
  public void setChange_time(Date _change_time){
    change_time = _change_time;
  }
  public void setIsNew(boolean b){
	isNew = b;    
  }
  //Getters

  public int getId(){
    return id;
  }
  public int getSystem_id(){
    return system_id;
  }
  public int getStatus_id(){
    return status_id;
  }
  public Date getUpdate_time(){
    return update_time;
  }
  public Date getChange_time(){
    return change_time;
  }
	public Boolean isNew() {
		return isNew;
  }
	//Data Access Methods
	//Data Access Methods
	public boolean save(Connection _connection){
		return(save(_connection,true,true));
	}
	public boolean save(Connection _connection,boolean doValidate,boolean executeHooks){
		if(doValidate){
			try {
				if(!mh.validate(_connection)){
					return(false);
				}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			if(!isReadyToSave()){
				return(false);
			}
		}
		if(executeHooks){
			try {
				if(isNew()){
					if(!mh.beforeInsert(_connection)){return(false);}
				}else{
					if(!mh.beforeUpdate(_connection)){return(false);}
				}
				if(!mh.beforeSave(_connection)){return(false);}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			//Actual db update
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				  	if(!mh.afterInsert(_connection)){return(false);}
				  	if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
					if(!mh.afterUpdate(_connection)){return(false);}
					if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}else{
			//Actual db operation without hooks
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}
		return true;
	}
	public boolean destroy(Connection _connection,boolean executeHooks){
		if(executeHooks){
			if(!mh.beforeDestroy(_connection)){return(false);}
		}
		if(!delete(_connection)){return(false);}
		if(executeHooks){
			if(!mh.afterDestroy(_connection)){return(false);}
		}
		return(true);
	}
	public boolean destroy(Connection _connection){
		return(destroy(_connection,true));
	}

	//Private Data Acess utility Methods
	private boolean insert(Connection _connection){
    Query query;
    query = _connection.createQuery(insertSql(),true);
	
	
			query = query.addParameter("system_idP",system_id);
	query = query.addParameter("status_idP",status_id);
	query = query.addParameter("update_timeP",update_time);
	query = query.addParameter("change_timeP",change_time);

		
    id = (int) query.executeUpdate().getKey();
    
    return(true);
	}
	private boolean update(Connection _connection){
    Query query;
    query = _connection.createQuery(updateSql());
	
	query = query.addParameter("idP",id);
	
	query = query.addParameter("system_idP",system_id);
	
	query = query.addParameter("status_idP",status_id);
	
	query = query.addParameter("update_timeP",update_time);
	
	query = query.addParameter("change_timeP",change_time);

    query.executeUpdate();
    return(true);
	}
	private boolean delete(Connection _connection){
		Query query;
		query = _connection.createQuery(deleteSql());
    query.addParameter("idP",id);
    query.executeUpdate();
    return(true);
	}
  private String insertSql(){
    String  querySql = "insert into system_status( ";
      querySql += "system_id,";
      querySql += "status_id,";
      querySql += "update_time,";
      querySql += "change_time)";
  	
    querySql += "values (";
      querySql += ":system_idP,";
      querySql += ":status_idP,";
      querySql += ":update_timeP,";
      querySql += ":change_timeP)"; 
  	
    return querySql;
  }
  private String updateSql(){
    String  querySql = "update system_status set ";
      querySql += "system_id = :system_idP, " ;
      querySql += "status_id = :status_idP, " ;
      querySql += "update_time = :update_timeP, " ;
      querySql += "change_time = :change_timeP ";
  	
			querySql += " where id = :idP";
	
	
	
	
	
	
    
   return querySql;
  }
  private String deleteSql(){
			String querySql = "delete from system_status where id = :idP";
	
	
	
	
	
	
  	
  	return querySql;
  }

}
