package org.sisiya.ui.controllers;

import java.util.HashMap;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.Constants;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.FlashHelper;
import org.sisiya.ui.helpers.I18nHelper;
import org.sisiya.ui.models.Upload;
import org.sisiya.ui.models.User;
import org.sql2o.Connection;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import spark.Request;
import spark.Response;

public class UploadsController  extends ApplicationController implements CrudController{

	public UploadsController(String _path) {
		super(_path);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String indexAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String newAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createAction(Request req, Response res) {
		if(respondToJson(req)){
			JsonObject object = new JsonParser().parse(req.body()).getAsJsonObject();
			if (object.isJsonArray()) {
				Connection c = Application.db.beginTransaction();
				JsonObject resj = new JsonObject();
				object.getAsJsonArray().forEach((e)->{
					JsonObject jo = e.getAsJsonObject();
					
					Upload upload = new Upload();
					upload.setModel_name(jo.get("model").getAsString());
					upload.setModel_id(jo.get("model_id").getAsInt());
					upload.setName(jo.get("name").getAsString());
					upload.setBlob(jo.get("data").getAsString());
					upload.setType(jo.get("type").getAsString());
					if(upload.save(c)){
						res.status(200);
						resj.addProperty("success", true);
						res.body(resj.toString());				
					}else{
						resj.addProperty("success", false);
						resj.addProperty("message", "err: "+upload.getErrors().get(0).getErr());
						res.body(resj.toString());
						res.status(200);
						return;
					}
				});
				if (resj.get("success").getAsBoolean()) {
					c.commit();
				}else{
					c.rollback();
				}
				c.close();
			}
			if (object.isJsonObject()) {
				Upload upload = new Upload();
				upload.setModel_name(object.get("model").getAsString());
				upload.setModel_id(object.get("model_id").getAsInt());
				upload.setName(object.get("name").getAsString());
				upload.setBlob(object.get("data").getAsString());
				upload.setType(object.get("type").getAsString());
				Connection c = Application.db.open();
				JsonObject resj = new JsonObject();
				//TimeUnit.SECONDS.sleep(3);
				if(upload.save(c)){
					res.status(200);
					resj.addProperty("success", true);
					res.body(resj.toString());				
				}else{
					resj.addProperty("success", false);
					resj.addProperty("message", "err: "+upload.getErrors().get(0).getErr());
					res.body(resj.toString());
					res.status(200);
				}
				return "";					
			}
		
		}
		return "";
	}

	@Override
	public String showAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String editAction(Request req, Response res) {
		return "";
	}

	@Override
	public String updateAction(Request req, Response res) {
		return "";
	}

	@Override
	public String deleteAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

}