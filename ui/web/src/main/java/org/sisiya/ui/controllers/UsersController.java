package org.sisiya.ui.controllers;

import java.util.List;

import org.sisiya.ui.Application;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.FlashHelper;
import org.sisiya.ui.models.Upload;
import org.sisiya.ui.models.User;
import org.sql2o.Connection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ca.krasnay.sqlbuilder.SelectBuilder;
import spark.Request;
import spark.Response;

public class UsersController  extends ApplicationController implements CrudController{

	public UsersController(String _path) {
		super(_path);
	}

	@Override
	public String indexAction(Request req, Response res) {
		if(respondToHtml(req)){
			model = getDefaultModel(req.session());
			jsmodel = getDefaultModelForJsAsset(req.session());
			model.put("customjs", ApplicationHelper.parseFreemarkerAsset("assets/users.js",jsmodel));
			return ApplicationHelper.render(model, viewPath("index"));			
		}
		if (respondToJson(req)) {
			Gson gson = new Gson();
			User user = new org.sisiya.ui.models.User();
			List<User> users;

			SelectBuilder sb = new SelectBuilder().column("*").from("users");
			req.queryParams().forEach((p)->{
				if(req.queryParams(p)!=null && !req.queryParams(p).isEmpty()){
					if(user.getField(p).getType()=="String"){
						sb.where(p+" LIKE "+"'%"+req.queryParams(p)+"%'");
					}else{
						sb.where(p+" = "+req.queryParams(p));
					}
				}
			});

			Connection conn = Application.db.open();
			users = conn.createQuery(sb.toString()).executeAndFetch(User.class);
			conn.close();
			res.body(gson.toJson(users));
			return("");				
		}
		return "";
	}

	@Override
	public String newAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String showAction(Request req, Response res) {
		if (respondToHtml(req)) {
			model = getDefaultModel(req.session());
			User user = User.findUser(Application.db.open(), req.params(":id"));
			if(user!=null){
				model.put("user", user);
			}else{
				FlashHelper.error(req.session(), "User not found!");
				res.redirect("/labels",307);
			}
			return ApplicationHelper.render(model, viewPath("show"));
		}
		return "";
	}

	@Override
	public String editAction(Request req, Response res) {
		if (respondToHtml(req)) {
			model = getDefaultModel(req.session());
			Connection c = Application.db.open();
			User user = User.findUser(c, req.params(":id"));
			if(user!=null){
				Upload u = user.getProfilePicture(c);
				if (u!=null) {
					user.setAvatar(u.getUrl("thumb"));
				}
				model.put("user", user);
				
			}else{
				FlashHelper.error(req.session(), "User not found!");
				res.redirect("/users",307);
			}
			jsmodel = getDefaultModelForJsAsset(req.session());
			model.put("customjs", ApplicationHelper.parseFreemarkerAsset("assets/users-edit.js", jsmodel));
			return ApplicationHelper.render(model, viewPath("edit"));			
		}
		return "";
	}

	@Override
	public String updateAction(Request req, Response res) {
		if (respondToJson(req)){
			JsonObject object = new JsonParser().parse(req.body()).getAsJsonObject();
			Connection c = Application.db.open();
			if(object.get("avatar")!=null){
				User user = User.findUser(c, object.get("id").getAsString());
				user.setAvatar(user.getProfilePicture(c).getUrl("thumb"));
				if ((int)(req.session().attribute("user_id"))==user.getId()) {
					ApplicationHelper.setCurrentUser(req.session(), user);
				}
				res.status(200);
			}
			c.close();
			return "";			
		}
		return "";
	}

	@Override
	public String deleteAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

}