package org.sisiya.ui.helpers;

import java.util.List;

import org.sisiya.ui.Application;

import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class I18nHelper implements TemplateMethodModelEx{

	@Override
	public SimpleScalar exec(List args) throws TemplateModelException {
			// TODO Auto-generated method stub
			//String	tmp=(String) args.get(0);
			String	label = new StringBuilder().append(""+args.get(0)).toString();
			String	desc = Application.i18n.t(label);
			if(args.size()==2){
				SimpleNumber n = (SimpleNumber) args.get(1);
				int lang_id = n.getAsNumber().intValue();
				desc = Application.i18n.t(label,lang_id);
			}
			//String tmp = new StringBuilder().append(Application.i18n.t((String) args.get(0))).toString();
			return(SimpleScalar.newInstanceOrNull(desc));
		}
	}
