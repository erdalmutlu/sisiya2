package org.sisiya.ui.helpers;

public class BackToQueue {
	private String[] paths;
	private int index = 0;
	private int maxSize;
	
	public BackToQueue(int size) {
		maxSize = size;
		paths = new String[maxSize];
	}
	public void add(String path){
		
		if(index>=maxSize){
			//Remove the first element and replace all elements in the  array
			for (int i = 0; i < paths.length; i++) {
				paths[i]=paths[i+1];
			}
			paths[maxSize-1]=path;
		}else{
			if (index!=0){
				
				if(!paths[index-1].equals(path)){
					paths[index]=path;
					index+=1;	
				}			
			}else{
				paths[index]=path;
				index+=1;					
			}
		}
	}
	private void remove(String path){
		//Remove the path from the tail
		if(index>=maxSize){index = maxSize-1;}
		for (int i = index-1; i>=0; i--) {
			if(!paths[i].equals(path)){
				index = i;
				return;
			}else{
				paths[i]=null;
				if(i==0){
					index = 0;
				}
			}
			
		}
	}
	public String getPreviousPath(String current_path){
		remove(current_path);
		return(paths[index]);
	}

}
