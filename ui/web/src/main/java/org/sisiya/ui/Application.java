/**
 * 
 */
package org.sisiya.ui;

import static spark.Spark.*;

import java.nio.file.Paths;

import org.sisiya.ui.lib.I18n;
import org.sql2o.Sql2o;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
/**
 * @author fatihgenc
 *
 */
public class Application {
	public static Sql2o db;
	public static I18n i18n;
	public static ApplicationRouter router;
	
	private static HikariDataSource ds;
	
	
	public static void main(String[] args) {
		init();
	}
	public static void init(){
		port(8080);
		staticFiles.location("/public/"); // Static file
		staticFiles.externalLocation(Paths.get("uploads").toAbsolutePath().toString());
		HikariConfig config = new HikariConfig("./config/db.properties");
		config.setMaximumPoolSize(20);
		config.setMinimumIdle(5);
		config.setIdleTimeout(60000);
		ds = new HikariDataSource(config);
		db = new Sql2o(ds);
		i18n = new I18n(db.open());
		router = new ApplicationRouter();
	}

}
