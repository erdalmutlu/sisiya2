package org.sisiya.ui.standart;

import org.sql2o.Connection;

public interface ModelHooks {
	public boolean beforeSave(Connection _connection);
	public boolean afterSave(Connection _connection);
	public boolean beforeInsert(Connection _connection);
	public boolean afterInsert(Connection _connection);
	public boolean beforeUpdate(Connection _connection);
	public boolean afterUpdate(Connection _connection);
	public boolean beforeDestroy(Connection _connection);
	public boolean afterDestroy(Connection _connection);
	public boolean validate(Connection _connection);
}
