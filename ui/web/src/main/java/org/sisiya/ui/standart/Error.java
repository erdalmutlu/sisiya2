package org.sisiya.ui.standart;

public class Error {
	private String code;
	private String err;
	
	public Error(String code,String err){
		setCode(code);
		setErr(err);
	}
	public Error(){
		
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getErr() {
		return err;
	}
	public void setErr(String err) {
		this.err = err;
	}
}