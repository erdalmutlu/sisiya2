package org.sisiya.ui;

public final class Constants {
	public static final String FLASH_ERROR = "flasherror";
	public static final String FLASH_INFO = "flashinfo";
	public static final String FLASH_WARNING = "flashwarning";
	public static final String FLASH_SUCCESS = "flashsuccess";
	
	public static final String RESPOND_TO = "respond_to";
	public static final String RESPOND_TO_HTML = "application/html";
	public static final String RESPOND_TO_JSON = "application/json";
}
