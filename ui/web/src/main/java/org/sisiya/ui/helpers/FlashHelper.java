package org.sisiya.ui.helpers;

import org.sisiya.ui.Constants;

import spark.Session;

public class FlashHelper {
	public static void success(Session session,String message){
		session.attribute(Constants.FLASH_SUCCESS,message);
	}
	public static void error(Session session,String message){
		session.attribute(Constants.FLASH_ERROR,message);
	}
	public static void warning(Session session,String message){
		session.attribute(Constants.FLASH_WARNING,message);
	}
	public static void info(Session session,String message){
		session.attribute(Constants.FLASH_INFO,message);
	}
}
