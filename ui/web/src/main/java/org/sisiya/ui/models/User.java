package org.sisiya.ui.models;
import java.util.List;

import org.sisiya.ui.helpers.PasswordHelper;
import org.sisiya.ui.standart.Error;
import org.sisiya.ui.standart.ModelHooks;
import org.sql2o.Connection;

public class User extends org.sisiya.ui.standart.User implements ModelHooks{
	private String	password,password_confirmation;
	private Upload 	profile_picture;
	private String 	avatar = "";
	public User() {
		// TODO Auto-generated constructor stub
		registerHooks(this);
	}
	@Override
	public boolean beforeSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeInsert(Connection _connection) {
		// TODO Auto-generated method stub
		try {
			password_hash = PasswordHelper.getSaltedHash(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			errors.add(new Error("-1", e.getMessage()));
			return false;
		}
		return true;
	}

	@Override
	public boolean afterInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean validate(Connection _connection) {
		// TODO Auto-generated method stub
		if(isNew()){
			return(password == password_confirmation ? true : false);
		}
		return true;
	}
	//
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Upload	getProfilePicture(Connection _connection){
		if(Upload.findUpload(_connection, "User", this.getId()).size()==0){
			return null;
		}
		return Upload.findUpload(_connection, "User", this.getId()).get(0);
	}
//	public boolean setProfilePicture(Connection _connection,Upload upload) {
//		Upload upl = getProfilePicture(_connection);
//		if(upl!=null){
//			upl.setIsNew(false);
//			upl.setName(upload.getName());
//			upl.setBlob(upload.getBlob());
//			upl.setType(upload.getType());
//		}else{
//			upl = upload;
//		}
//		if(!upl.save(_connection)){
//			return false;
//		}
//		return true;
//	}
	//Static Functions
	public static User findByEmail(Connection _connection,String email){
		//User user = new User();
		String sql = "select * from users where email=:email";
		List<User> users = _connection.createQuery(sql).addParameter("email", email).executeAndFetch(User.class);
		_connection.close();
		if(users.size()>0){
			return users.get(0);
		}
		return null;
	}
	public static User findByUserName(Connection _connection,String username){
		//User user = new User();
		String sql = "select * from users where username=:username";
		List<User> users = _connection.createQuery(sql).addParameter("username", username).executeAndFetch(User.class);
		_connection.close();
		if(users.size()>0){
			return users.get(0);
		}
		return null;
	}
	public static User findByUserId(Connection _connection,int id){
		//User user = new User();
		String sql = "select * from users where id=:id";
		List<User> users = _connection.createQuery(sql).addParameter("id", id).executeAndFetch(User.class);
		_connection.close();
		if(users.size()>0){
			return users.get(0);
		}
		return null;
	}
	public static User findUser(Connection _connection,String namemailid){
		User user = User.findByEmail(_connection, namemailid);
		if(user!=null){return user;}
		user = User.findByUserName(_connection, namemailid);
		if(user!=null){return user;}
		try {
			user = User.findByUserId(_connection, Integer.parseInt(namemailid));
		} catch (Exception e) {
			return(null);
		}
		
		if(user!=null){return user;}
		return null;
	}
}
