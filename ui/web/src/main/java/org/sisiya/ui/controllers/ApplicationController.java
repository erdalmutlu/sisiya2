package org.sisiya.ui.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.Constants;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.HtmlHelper;
import org.sisiya.ui.helpers.I18nHelper;
import org.sisiya.ui.helpers.PathHelper;
import org.sisiya.ui.models.Language;

import spark.Request;
import spark.Session;

import static spark.Spark.*;

public class ApplicationController {
	
	protected String path;
	protected Map<String, Object> model;
	protected Map<String, Object> jsmodel;
	
	public ApplicationController(String _path){
		
		path = _path;
		after((req, res) -> {
		    req.session().attribute("flash-error", "");
		    req.session().attribute("flash-warning", "");
		    req.session().attribute("flash-info", "");
		});
		before((req, resp) -> {
			String loginPaths[] = {"/login","/sessions","/logout","/sessions/new"};
			ApplicationHelper.addToSessionHistory(req.session(), req.pathInfo());
			if(!Arrays.asList(loginPaths).contains(req.pathInfo())){
			if(!ApplicationHelper.loggedIn(req.session())){
				resp.redirect(Application.router.loginPath(),307);
			}else{
				if(req.session().attribute("current_lang")!=null){
					Application.i18n.setCurrentLang(Integer.parseInt(req.session().attribute("current_lang")));
				}
			}}
		});
	}

	public String getPath(){
		return path;
	}
	public String viewPath(String view){
		return "/views/"+path+"/"+view;
	}
	public Map<String, Object> getDefaultModel(Session session){		
		model = new HashMap<>();
		model.put("title",Application.i18n.t("l000000129"));
		model.put("favicon", "/img/favicon.ico");
		model.put("logo", "/img/sisiya.png");
		model.put("i18nhelper",new I18nHelper());
		model.put("htmlhelper",new HtmlHelper());
		model.put("pathhelper",new PathHelper());
		model.put("langs", Language.all());
		model.put("currentlang", Application.i18n.getCurrentLang());
		model.put("current_user", session.attribute("current_user"));
		addFlashMessagesToModel(model, session);
		return(model);
	}
	public Map<String, Object> getDefaultModelForJsAsset(Session session){		
		jsmodel = new HashMap<>();
		jsmodel.put("i18nhelper", new I18nHelper());
		jsmodel.put("current_user", session.attribute("current_user"));
		return(jsmodel);
	}
	public void addFlashMessagesToModel(Map<String, Object> model,Session session){
		if(session.attribute(Constants.FLASH_ERROR)!=null){
			model.put(Constants.FLASH_ERROR,session.attribute(Constants.FLASH_ERROR));
			session.attribute(Constants.FLASH_ERROR,null);
		}else if(session.attribute(Constants.FLASH_INFO)!=null){
			model.put(Constants.FLASH_INFO,session.attribute(Constants.FLASH_INFO));
			session.attribute(Constants.FLASH_INFO,null);
		}else if(session.attribute(Constants.FLASH_WARNING)!=null){
			model.put(Constants.FLASH_WARNING,session.attribute(Constants.FLASH_WARNING));
			session.attribute(Constants.FLASH_WARNING,null);
		}else if(session.attribute(Constants.FLASH_SUCCESS)!=null){
			model.put(Constants.FLASH_SUCCESS,session.attribute(Constants.FLASH_SUCCESS));
			session.attribute(Constants.FLASH_SUCCESS,null);
		}
	}
	public boolean respondToJson(Request req){
		return respondTo(req, Constants.RESPOND_TO_JSON);
	}
	public boolean respondToHtml(Request req){
		return respondTo(req, Constants.RESPOND_TO_HTML);
	}
	private boolean respondTo(Request req,String type){
		if(req.attribute(Constants.RESPOND_TO).equals(type)){
			return true;
		}
		return false;		
	}

}