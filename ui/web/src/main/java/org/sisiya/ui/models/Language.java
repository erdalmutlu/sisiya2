package org.sisiya.ui.models;
import java.util.List;

import org.sisiya.ui.Application;
import org.sisiya.ui.standart.ModelHooks;
import org.sql2o.Connection;

public class Language extends org.sisiya.ui.standart.Language implements ModelHooks{
	public Language() {
		// TODO Auto-generated constructor stub
		registerHooks(this);
	}
	@Override
	public boolean beforeSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean validate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}
	//Static methods

	public static List<Language> all(){
		Connection c = Application.db.open();
		List<Language> objects = c.createQuery("select * from languages").executeAndFetch(Language.class);
		c.close();
		return objects;
	}
}
