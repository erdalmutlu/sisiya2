package org.sisiya.ui.controllers;

import spark.Request;
import spark.Response;

public interface CrudController {
	public String indexAction(Request req,Response res);
	public String newAction(Request req,Response res);
	public String createAction(Request req,Response res);
	public String showAction(Request req,Response res);
	public String editAction(Request req,Response res);
	public String updateAction(Request req,Response res);
	public String deleteAction(Request req,Response res);
}