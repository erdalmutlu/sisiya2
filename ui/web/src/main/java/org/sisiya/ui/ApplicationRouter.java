package org.sisiya.ui;

import static spark.Spark.get;

import org.sisiya.ui.controllers.SessionsController;
import org.sisiya.ui.controllers.LabelsController;
import org.sisiya.ui.controllers.ServicesController;
import org.sisiya.ui.controllers.StatusController;
import org.sisiya.ui.controllers.SystemStatusController;
import org.sisiya.ui.controllers.SystemTypesController;
import org.sisiya.ui.controllers.UploadsController;
import org.sisiya.ui.controllers.UsersController;
import org.sisiya.ui.lib.Router;

public class ApplicationRouter extends Router{
	public ApplicationRouter() {
		super();
		routes.put("users", new UsersController("users"));
		routes.put("sessions", new SessionsController("sessions"));
		routes.put("systemstatus", new SystemStatusController("systemstatus"));
		routes.put("labels", new LabelsController("labels"));
		routes.put("uploads", new UploadsController("uploads"));
		routes.put("system_types", new SystemTypesController("system_types"));
		routes.put("status", new StatusController("status"));
		routes.put("services", new ServicesController("services"));
		initStandartRoutes();
		initCustomRoutes();
	}
	//Define Paths
	public String loginPath() {
		return("/login");
	}
	public String rootPath() {
		return("/");
	}
	public String logoutPath() {
		return("/logout");
	}	
	//Define custom routes
	public void initCustomRoutes() {
		get(rootPath(),(req,res) -> {	//Root path
			req.attribute(Constants.RESPOND_TO,Constants.RESPOND_TO_HTML);
			return(routes.get("labels").indexAction(req, res));
		});		
		get(loginPath(),(req,res) -> {
			return(routes.get("sessions").newAction(req, res));
		});
		get(logoutPath(),(req,res) -> {
			return(routes.get("sessions").deleteAction(req, res));
		});
	}
}
