package org.sisiya.ui.controllers;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.FlashHelper;
import org.sisiya.ui.helpers.PasswordHelper;
import org.sisiya.ui.models.User;
import org.sisiya.ui.models.Upload;
import org.sql2o.Connection;

import spark.Request;
import spark.Response;

public class SessionsController  extends ApplicationController implements CrudController{

	public SessionsController(String _path) {
		super(_path);
	}

	@Override
	public String indexAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String newAction(Request req, Response res) {
		model = getDefaultModel(req.session());
		model.put("layout", "login");
		model.put("customjs", ApplicationHelper.parseFreemarkerAsset("assets/login.js",null));
		try {
			model.put("customcss",new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("assets/login.css").toURI()))));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
		return ApplicationHelper.render(model, viewPath("login"));
	}

	@Override
	public String createAction(Request req, Response res) {
		Map<String, String>  params = ApplicationHelper.getParams(req.body());
		Connection c = Application.db.open();
		User user = User.findByEmail(c,params.get("email"));
		if(user!=null && !params.get("password").isEmpty()){
			try {
				if(PasswordHelper.check(params.get("password"), user.getPassword_hash())){
					Upload u = user.getProfilePicture(c);
					if(u!=null){
						user.setAvatar(u.getUrl("thumb"));
					}
					ApplicationHelper.setCurrentUser(req.session(), user);
					req.session().attribute("current_lang",params.get("language_id"));
					c.close();
					FlashHelper.success(req.session(), Application.i18n.t("l000000044",Integer.parseInt(params.get("language_id"))));	//Successfuly logged in
					res.redirect(Application.router.rootPath());
					return "";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				FlashHelper.error(req.session(),e.getMessage());	//Invalid login
				res.redirect(Application.router.loginPath());
				e.printStackTrace();
				return "";
			}
		}
		c.close();
		FlashHelper.error(req.session(),Application.i18n.t("l000000130"));	//Invalid login
		res.redirect("login");
		return "";
	}

	@Override
	public String showAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String editAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteAction(Request req, Response res) {
		if(ApplicationHelper.loggedIn(req.session())){
			req.session().attribute("user_id", null);
			req.session().invalidate();
			FlashHelper.info(req.session(),Application.i18n.t("l000000050"));	//logged out successfully
			res.redirect(Application.router.loginPath());
		}else{
			FlashHelper.warning(req.session(),Application.i18n.t("l000000028"));	//already logged out
			res.redirect(Application.router.loginPath());		
		}
		return "";
	}

}
