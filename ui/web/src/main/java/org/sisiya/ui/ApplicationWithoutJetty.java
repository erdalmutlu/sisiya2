package org.sisiya.ui;

import static spark.Spark.port;
import static spark.Spark.staticFiles;

import org.sisiya.ui.controllers.SessionsController;
import org.sisiya.ui.controllers.SystemStatusController;
import org.sisiya.ui.controllers.UsersController;
import org.sisiya.ui.lib.I18n;
import org.sql2o.Sql2o;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ApplicationWithoutJetty implements spark.servlet.SparkApplication{
	public static Sql2o db;
	public static I18n i18n;
	private static HikariDataSource ds;
	@Override
	public void init() {
		Application.init();
	}

}
