package org.sisiya.ui.helpers;

import java.util.List;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import static j2html.TagCreator.*;

public class HtmlHelper implements TemplateMethodModelEx{

	@Override
	public SimpleScalar exec(List arg0) throws TemplateModelException {
		String s = div("test").render();
		return (SimpleScalar.newInstanceOrNull(s));
	}
}
