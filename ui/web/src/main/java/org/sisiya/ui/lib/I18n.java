package org.sisiya.ui.lib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.sisiya.ui.Application;
import org.sisiya.ui.models.Label;
import org.sisiya.ui.models.Language;
import org.sql2o.Connection;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.emitter.ScalarAnalysis;
import org.yaml.snakeyaml.representer.Representer;


public class I18n {
	private	Map< String, Map<Integer,Label>> labels = new ConcurrentHashMap<>();	//thread safe
	private int currentLang;
	private final int defaultLang = 1;
	
	
	public  I18n(Connection conn) {
		loadTranslationsFromDb(conn);
		setCurrentLang(defaultLang);
	}
	public String t(String lbl){
		return t(lbl,currentLang);
	}
	public String t(String lbl,int lang_id) {
		if(labels.containsKey(lbl)){
			return(labels.get(lbl).get(lang_id).getStr());
		}
		return("Translation not found for "+lbl);
	}
	public void	loadTranslationsFromDb(Connection conn) {
		List <Label> lbls = conn.createQuery("select * from labels").executeAndFetch(Label.class);
		lbls.forEach((l) -> {
			if(labels.containsKey(l.getLabel())){
				labels.get(l.getLabel()).put(l.getLanguage_id(), l);
			}else{
				Map<Integer,Label> m = new HashMap<>();
				m.put(l.getLanguage_id(), l);
				labels.put(l.getLabel(),m);
			}
		});
		conn.close();
	}
	public void writeTranslationsToFile(){
		List<Label> labels = Label.all();
		Language.all().forEach((lan)->{
			Map<String, Object> root = new HashMap<String, Object>();
			Map<String, Object> data = new HashMap<String, Object>();
			labels.forEach((l)->{
				if(l.getLanguage_id() == lan.getId()){
					data.put(l.getLabel(), l.getStr());
				}
			});
			 root.put(lan.getCode(), data);
			 DumperOptions options = new DumperOptions() {};
			 options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
			 options.setPrettyFlow(true);
			 Yaml yaml = new Yaml(options);
			
			try {
				URI s = ClassLoader.getSystemResource("i18n/"+lan.getCode().toLowerCase()+".yml").toURI();
				yaml.dump(root,new FileWriter(new File(lan.getCode().toLowerCase()+".yml")));
			} catch (IOException | URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	public void loadTranslationsFromYaml(){
		Yaml yaml = new Yaml();
		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("lp", "l%");
		List<Label> ls = Label.where("label like :lp", params);
		Connection c = Application.db.beginTransaction();
		ls.forEach((l)->{
			l.destroy(c,false);
		});
		c.commit();
		c.close();
		Language.all().forEach((lan)->{
			Map<String,Object> res=null;
			try {
				res = (Map< String, Object>) yaml.load(new FileInputStream(new File("i18n/"+lan.getCode().toLowerCase()+".yml")));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			Connection cn = Application.db.beginTransaction();
			res.forEach((k,v)->{
				((Map<String, String>) v).forEach((j,z)->{
					//System.out.println(j+" : "+z);
					Label label = new Label();
					label.setLabel(j);
					label.setLanguage_id(lan.getId());
					label.setStr(z);
					
					label.save(cn,false,false);

				});
			});
			cn.commit();
			cn.close();
		});
		loadTranslationsFromDb(Application.db.open());
	}
	public int getCurrentLang() {
		return currentLang;
	}
	public void setCurrentLang(int current_lang) {
		this.currentLang = current_lang;
	}
	public void setCurrentLangToDefault() {
		this.currentLang = defaultLang;
	}
}
