package org.sisiya.ui.standart;

import java.util.ArrayList;

public class Field {
	  private String name;
	  private String type;
	  private String type2;
	  private String label;
	  private boolean isPrimaryKey;
	  private boolean isAutoIncrement;
	  private boolean isDerived;
	  private boolean isMandatory;
	  private ArrayList<Error> errors = new ArrayList<Error>();
	  
	  public Field(String _name,String _type,String _label,boolean ip,boolean ai){
		  setName(_name);
		  setType(_type);
		  setLabel(_label);
		  setAutoIncrement(ai);
		  setPrimaryKey(ip);
		  setDerived(false);
		  setMandatory(false);
	  }
	  public String getName(){
	    return name;
	  }
	  public String getType(){
	    return type;
	  }
	  public String getType2(){
		    return type2;
	  }	  
	  public void setName(String _name){
		name = _name;
	  }
	  public void setType(String _type){
		type = _type;
		switch (type) {
			case "int":
				setType2("int");
				break;
			case "String":
				setType2("text");
				break;
			case "Date":
				setType2("date");
				break;
			case "double":
				setType2("float");
				break;
			default:
				setType2("text");
				break;
		}
	  }
	  public void setType2(String _type2) {
		this.type2 = _type2;
	  }
	  public boolean hasError() {
		  return(!errors.isEmpty());
	  }
	  public void addError(Error error) {
		  errors.add(error);
	  }
	  public void resetErrors(){
		  errors.clear();
	  }
	  public String getLabel() {
		  return label;
	  }
	  public void setLabel(String label) {
		  this.label = label;
	  }
	  public boolean isDerived() {
		return isDerived;
	  }
	  public void setDerived(boolean isDerived) {
		this.isDerived = isDerived;
	  }
	  public boolean isMandatory() {
		return isMandatory;
	  }
	  public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	  }
	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}
	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}
	public boolean isAutoIncrement() {
		return isAutoIncrement;
	}
	public void setAutoIncrement(boolean isAutoIncrement) {
		this.isAutoIncrement = isAutoIncrement;
	}

}
