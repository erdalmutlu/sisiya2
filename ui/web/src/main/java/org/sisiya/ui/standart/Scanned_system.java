//Auto generated file
//Do not edit this file
package org.sisiya.ui.standart;
import org.sql2o.Connection;
import org.sql2o.Query;
import java.util.List;
import org.sisiya.ui.Application;

							
public class Scanned_system extends Model{

	//Fields  
  protected int id;
  protected int user_id;
  protected int system_type_id;
  protected String hostname;
  protected String fullhostname;
  protected String ip;
  protected String mac;

	private	ModelHooks mh;
	private Boolean isNew;
 //Constructer
  public Scanned_system(){
  	isNew = true;
  	//Default Constructer
    fields.add(new Field("id","int","scanned_system-id",true,true));
   fields.add(new Field("user_id","int","scanned_system-user_id",false,false));
   fields.add(new Field("system_type_id","int","scanned_system-system_type_id",false,false));
   fields.add(new Field("hostname","String","scanned_system-hostname",false,false));
   fields.add(new Field("fullhostname","String","scanned_system-fullhostname",false,false));
   fields.add(new Field("ip","String","scanned_system-ip",false,false));
   fields.add(new Field("mac","String","scanned_system-mac",false,false));
  }
  public void registerHooks(ModelHooks _mh){
	  mh = _mh;
  }
 //Setters

  public void setId(int _id){
    id = _id;
  }
  public void setUser_id(int _user_id){
    user_id = _user_id;
  }
  public void setSystem_type_id(int _system_type_id){
    system_type_id = _system_type_id;
  }
  public void setHostname(String _hostname){
    hostname = _hostname;
  }
  public void setFullhostname(String _fullhostname){
    fullhostname = _fullhostname;
  }
  public void setIp(String _ip){
    ip = _ip;
  }
  public void setMac(String _mac){
    mac = _mac;
  }
  public void setIsNew(boolean b){
	isNew = b;    
  }
  //Getters

  public int getId(){
    return id;
  }
  public int getUser_id(){
    return user_id;
  }
  public int getSystem_type_id(){
    return system_type_id;
  }
  public String getHostname(){
    return hostname;
  }
  public String getFullhostname(){
    return fullhostname;
  }
  public String getIp(){
    return ip;
  }
  public String getMac(){
    return mac;
  }
	public Boolean isNew() {
		return isNew;
  }
	//Data Access Methods
	//Data Access Methods
	public boolean save(Connection _connection){
		return(save(_connection,true,true));
	}
	public boolean save(Connection _connection,boolean doValidate,boolean executeHooks){
		if(doValidate){
			try {
				if(!mh.validate(_connection)){
					return(false);
				}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			if(!isReadyToSave()){
				return(false);
			}
		}
		if(executeHooks){
			try {
				if(isNew()){
					if(!mh.beforeInsert(_connection)){return(false);}
				}else{
					if(!mh.beforeUpdate(_connection)){return(false);}
				}
				if(!mh.beforeSave(_connection)){return(false);}
			} catch (Exception e) {
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return(false);
			}
			//Actual db update
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				  	if(!mh.afterInsert(_connection)){return(false);}
				  	if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
					if(!mh.afterUpdate(_connection)){return(false);}
					if(!mh.afterSave(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}else{
			//Actual db operation without hooks
			if(isNew()){
				try {
					if(!insert(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}
			}else{
				try {
					if(!update(_connection)){return(false);}
				} catch (Exception e) {
					errors.add(new Error(e.getClass().getName(), e.getMessage()));
					return(false);				
				}			
			}
		}
		return true;
	}
	public boolean destroy(Connection _connection,boolean executeHooks){
		if(executeHooks){
			if(!mh.beforeDestroy(_connection)){return(false);}
		}
		if(!delete(_connection)){return(false);}
		if(executeHooks){
			if(!mh.afterDestroy(_connection)){return(false);}
		}
		return(true);
	}
	public boolean destroy(Connection _connection){
		return(destroy(_connection,true));
	}

	//Private Data Acess utility Methods
	private boolean insert(Connection _connection){
    Query query;
    query = _connection.createQuery(insertSql(),true);
	
	
			query = query.addParameter("user_idP",user_id);
	query = query.addParameter("system_type_idP",system_type_id);
	query = query.addParameter("hostnameP",hostname);
	query = query.addParameter("fullhostnameP",fullhostname);
	query = query.addParameter("ipP",ip);
	query = query.addParameter("macP",mac);

		
    id = (int) query.executeUpdate().getKey();
    
    return(true);
	}
	private boolean update(Connection _connection){
    Query query;
    query = _connection.createQuery(updateSql());
	
	query = query.addParameter("idP",id);
	
	query = query.addParameter("user_idP",user_id);
	
	query = query.addParameter("system_type_idP",system_type_id);
	
	query = query.addParameter("hostnameP",hostname);
	
	query = query.addParameter("fullhostnameP",fullhostname);
	
	query = query.addParameter("ipP",ip);
	
	query = query.addParameter("macP",mac);

    query.executeUpdate();
    return(true);
	}
	private boolean delete(Connection _connection){
		Query query;
		query = _connection.createQuery(deleteSql());
    query.addParameter("idP",id);
    query.executeUpdate();
    return(true);
	}
  private String insertSql(){
    String  querySql = "insert into scanned_systems( ";
      querySql += "user_id,";
      querySql += "system_type_id,";
      querySql += "hostname,";
      querySql += "fullhostname,";
      querySql += "ip,";
      querySql += "mac)";
  	
    querySql += "values (";
      querySql += ":user_idP,";
      querySql += ":system_type_idP,";
      querySql += ":hostnameP,";
      querySql += ":fullhostnameP,";
      querySql += ":ipP,";
      querySql += ":macP)"; 
  	
    return querySql;
  }
  private String updateSql(){
    String  querySql = "update scanned_systems set ";
      querySql += "user_id = :user_idP, " ;
      querySql += "system_type_id = :system_type_idP, " ;
      querySql += "hostname = :hostnameP, " ;
      querySql += "fullhostname = :fullhostnameP, " ;
      querySql += "ip = :ipP, " ;
      querySql += "mac = :macP ";
  	
			querySql += " where id = :idP";
	
	
	
	
	
	
	
	
    
   return querySql;
  }
  private String deleteSql(){
			String querySql = "delete from scanned_systems where id = :idP";
	
	
	
	
	
	
	
	
  	
  	return querySql;
  }

}
