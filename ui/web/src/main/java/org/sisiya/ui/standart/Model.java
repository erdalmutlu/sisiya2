package org.sisiya.ui.standart;

import java.util.ArrayList;


public abstract class Model{
	protected ArrayList<Field> fields = new ArrayList<>();
	protected ArrayList<Error> errors = new ArrayList<>();
	protected ModelHooks modelHook;
	protected static final int UPDATE = 0;
	protected static final int INSERT = 1;
	protected String prefix,suffix;

	public Model(){
		prefix = "";
		suffix = "";		
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public ArrayList<Error> getErrors(){
		return(errors);
	}
	public ArrayList<Field> getFields(){
		return(fields);
	}
	public Field getField(String name) {
		for (Field f : fields) {
			if (f.getName().equals(name)) {
				return(f);
			}
		}	
		return(null);
	}
	/**
	 * @param error the error to add
	 */
	protected void addError(Error error) {
		errors.add(error);
	}
	
	/**
	 * Reset all errors
	 */	
	protected void resetErrors() {
		errors.clear();
		for (Field f : fields){
			f.resetErrors();
		}
	}
	/**
	 * @param field the error to add
	 */	
	protected void addField(Field field) {
		fields.add(field);
	}
	public Field getPrimarykey() {
		for (Field f : fields) {
			if (f.isPrimaryKey()) {
				return(f);
			}
		}	
		return(null);
	}
	/**
	 * Check if the model is ready to save
	 */	
	protected boolean isReadyToSave() {
		if(!errors.isEmpty()){
			return false;
		}
		for (Field f : fields){
			if(f.hasError()){
				return false;
			}
		}
		return true;
	}	
}
