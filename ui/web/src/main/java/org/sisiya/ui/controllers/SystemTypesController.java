package org.sisiya.ui.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sisiya.ui.Application;
import org.sisiya.ui.Constants;
import org.sisiya.ui.helpers.ApplicationHelper;
import org.sisiya.ui.helpers.I18nHelper;
import org.sisiya.ui.models.System_type;
import org.sql2o.Connection;

import com.google.gson.Gson;

import ca.krasnay.sqlbuilder.SelectBuilder;
import spark.Request;
import spark.Response;

public class SystemTypesController  extends ApplicationController implements CrudController{

	public SystemTypesController(String _path) {
		super(_path);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String indexAction(Request req, Response res) {
		if(respondToHtml(req)){
			model = getDefaultModel(req.session());
			jsmodel = getDefaultModelForJsAsset(req.session());
			model.put("customjs", ApplicationHelper.parseFreemarkerAsset("assets/labels.js",jsmodel));
			return ApplicationHelper.render(model, viewPath("index"));			
		}
		if(respondToJson(req)){
			Gson gson = new Gson();
			System_type st = new org.sisiya.ui.models.System_type();
			List<System_type> sts;

			SelectBuilder sb = new SelectBuilder().column("*").from("system_types");
			req.queryParams().forEach((p)->{
				if(req.queryParams(p)!=null && !req.queryParams(p).isEmpty()){
					if(st.getField(p).getType()=="String"){
						sb.where(p+" LIKE "+"'%"+req.queryParams(p)+"%'");
					}else{
						sb.where(p+" = "+req.queryParams(p));
					}
				}
			});

			Connection conn = Application.db.open();
			sts = conn.createQuery(sb.toString()).executeAndFetch(System_type.class);
			conn.close();
			res.body(gson.toJson(sts));
			return("");			
		}
		return "";
	}

	@Override
	public String newAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String showAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String editAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteAction(Request req, Response res) {
		// TODO Auto-generated method stub
		return null;
	}

}