package org.sisiya.ui.models;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;

import org.sisiya.ui.standart.Error;
import org.sisiya.ui.standart.ModelHooks;
import org.sql2o.Connection;

import net.coobird.thumbnailator.Thumbnails;
//Image size struct
class ImageSize
{
    public int	height;
    public int 	width;
    public ImageSize(int h,int w){
    	height = h;
    	width = w;
    }
 }

public class Upload extends org.sisiya.ui.standart.Upload implements ModelHooks{

	private static final	HashMap<String,Integer>	allowedUploadCountForModel = new HashMap<>();
	private static final	HashMap<String,List<String>>	allowedUploadTypesForModel = new HashMap<>();
	private	static final	HashMap<String,ImageSize> imageSizes = new HashMap<>();
	private static final 	List<String> imageTypes = Arrays.asList("image/png","image/jpeg","image/jpg","image/gif");
	private	static final	String	UPLOAD_LOCATION = "uploads";
	static {
		imageSizes.put("thumb",new ImageSize(100, 100));
		imageSizes.put("middle",new ImageSize(640, 480));
		imageSizes.put("large",new ImageSize(800, 600));
		//For user
		allowedUploadCountForModel.put("User", 1);
		allowedUploadTypesForModel.put("User", imageTypes);
	}
	
	
	public Upload() {
		registerHooks(this);
	}
	@Override
	public boolean beforeSave(Connection _connection) {
		// TODO Auto-generated method stub
		if(isImage()){
			File dir = Paths.get(UPLOAD_LOCATION, ""+getId()).toFile();
			OutputStream stream;
			if(!dir.exists()){dir.mkdir();}
			//create original image dir
			File origDir = Paths.get(UPLOAD_LOCATION, getId()+"","org").toFile();
			if(!origDir.exists()){origDir.mkdir();}
			//write original image
			byte[] data = Base64.getDecoder().decode(blob.split(",")[1]);
			try {
				stream = new FileOutputStream(Paths.get(origDir.toString(), name).toFile());
				stream.write(data);
				stream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				errors.add(new Error(e.getClass().getName(), e.getMessage()));
				return false;
			}
			imageSizes.forEach((k,v)->{
				try {
					BufferedImage originalImage = ImageIO.read(Paths.get(origDir.toString(), name).toFile());
					Path path = Paths.get(dir.toString(),k);
					if(!path.toFile().exists()){path.toFile().mkdir();}
					path = Paths.get(path.toString(), name);
					Thumbnails.of(originalImage).size(v.width, v.height).toFile(path.toString());
					
				} catch (IOException e) {
					e.printStackTrace();
					errors.add(new Error(e.getClass().getName(),e.getMessage()));
				}
			});
		}

		
		return true;
	}

	@Override
	public boolean afterSave(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterInsert(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterUpdate(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean beforeDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean afterDestroy(Connection _connection) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean validate(Connection _connection) {
		if(!this.model_name.isEmpty()){
			//Check Allowed file types
			if(getAllowedUploadTypesForModel().containsKey(this.model_name)){
				List<String> list = getAllowedUploadTypesForModel().get(this.model_name);
				if(!list.contains(type.trim())){
					errors.add(new Error("1", "file type not allowed"));
					return false;
				}
			}
			//Check for allowed upload count
			if(getAllowedUploadCountForModel().containsKey(this.model_name)){
				int count = getAllowedUploadCountForModel().get(this.model_name);
				if(count == 1){
					if(!Upload.findUpload(_connection, this.model_name,this.model_id).isEmpty()){
						Upload exsisting = Upload.findUpload(_connection, this.model_name,this.model_id).get(0);
						this.setId(exsisting.getId());
						this.setIsNew(false);
					}
				}
			}
		}
		return true;
	}
	public boolean isImage(){
		if(imageTypes.contains(type.trim())){
			return true;
		}
		return false;
	}
	public String getUrl(String type){
		return "/"+getId()+"/"+type+"/"+getName();
	}
	public static HashMap<String, Integer> getAllowedUploadCountForModel() {
		return allowedUploadCountForModel;
	}
	public static HashMap<String, List<String>> getAllowedUploadTypesForModel() {
		return allowedUploadTypesForModel;
	}
	//Static functions
	public static List<Upload> findUpload(Connection _connection,String	model,int id){
		List<Upload> uploads;
		uploads = _connection.createQuery("select * from uploads where model_name=:mdl and model_id=:mid").addParameter("mdl", model).addParameter("mid", id).executeAndFetch(Upload.class);
		return uploads;
	}
}
