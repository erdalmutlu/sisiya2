create table languages (
	id	integer 	not null,
	str	varchar(255)	not null,
	code	varchar(16)	not null,
	charset	varchar(32)	not null,
	primary key(id)
);

create table labels (
	language_id	integer		not null references languages(id),
	label		varchar(255)	not null,
	str		text		not null,
	primary key(language_id, label)
);
create unique index languages_label on labels(language_id, label);
alter table labels add constraint fk_labels_language_id foreign key(language_id) references languages(id);

create table system_types (
	id	serial,
	label	varchar(255)	not null,
	primary key(id)
);
create unique index system_types_label on system_types(label);

create table services (
	id	integer		not null,
	label	varchar(255)	not null,
	primary key(id)
);
create unique index services_label on services(label);

create table status (
	id	integer		not null,
	label	varchar(255)	not null,
	primary key(id)
);
create unique index status_label on status(label);

create table properties (
	id	serial,
	code	varchar(255)	not null,
	label	varchar(255)	not null,
	value	text		not null,
	primary key(id)
);
create unique index properties_code_label on properties(code, label);

create table alert_types (
	id	serial,
	label	varchar(255)	not null,
	primary key(id)
);
create unique index alert_types_label on alert_types(label);

create table users (
	id		serial,
	username	varchar(32)	not null,
	password_hash	varchar(255)	not null,
	name		varchar(64)	not null,
	surname		varchar(64)	not null,
	email		varchar(128)	not null,
	temp_token		varchar(255),
	language_id		integer,
	last_login_ip		varchar(255),
	current_login_ip		varchar(255),
	failed_login_count		integer,
	login_count		integer,
	last_login_at	timestamp,
	current_login_at timestamp,
	last_request_at	timestamp,
	role_id	integer,
	create_date	timestamp,
	temp_token_created_at	timestamp,
	primary key(id)
);
create unique index users_username on users(username);
create unique index users_email on users(email);

create table locations (
	id		serial,
	sort_id		integer			not null,
	label		varchar(255)		not null,
	latitude	double precision,
	longitude	double precision,
	primary key(id)
);
create unique index locations_label on locations(label);

create table systems (
	id		serial,
	enabled		boolean		not null,
	system_type_id	integer		not null references system_types(id),
	location_id	integer		not null references locations(id),
	hostname	varchar(255)	not null,
	full_hostname	varchar(255)	not null,
	is_critical	boolean		not null,
	ip		varchar(255)	not null,
	mac		varchar(255)	not null,
	primary key(id)
);
create unique index systems_hostname on systems(hostname);
create unique index systems_mac on systems(mac);

create table system_alerts (
	user_id		integer		not null references users(id),
	system_id	integer		not null references systems(id),
	alert_type_id	integer		not null references alert_types(id),
	status_id	integer		not null references status(id),
	frequency	integer		not null,
	enabled		boolean		not null,
	last_alert_time	timestamp,
	primary key(user_id, system_id, alert_type_id, status_id)
);

create table system_service_alerts (
	user_id		integer		not null references users(id),
	system_id	integer		not null references systems(id),
	service_id	integer		not null references services(id),
	alert_type_id	integer		not null references alert_types(id),
	status_id	integer		not null references status(id),
	frequency	integer		not null,
	enabled		boolean		not null,
	last_alert_time	timestamp,
	primary key(user_id, system_id, service_id, alert_type_id, status_id)
);

create table scanned_systems (
	useri_id		integer		not null references users(id),
	system_type_id		integer		not null references system_types(id),
	hostname		varchar(32)	not null,
	fullhostname		varchar(64)	not null,
	ip			varchar(255)	not null,
	mac			varchar(255)	not null,
	primary key(useri_id, hostname)
);

create table system_service_status (
	id		serial,
	system_id	integer		not null references systems(id),
	service_id	integer		not null references services(id),
	status_id	integer		not null references status(id),
	update_time	timestamp	not null,
	change_time	timestamp	not null,
	expire		integer		not null,
	str		text		not null,
	primary key(id)
);
create unique index system_service_status_system_id_service_id on system_service_status(system_id,service_id);

create table system_service_status_lines (
	system_service_status_id	integer		not null references system_service_status(id),
	line_no				integer		not null,
	entry_type			varchar(255)	not null,
	entry_name			varchar(255)	not null,
	entry_value			varchar(255)	not null,
	primary key(system_service_status_id, line_no)
);

create table system_status (
	id		serial,
	system_id	integer		not null references systems(id),
	status_id	integer		not null references status(id),
	update_time	timestamp	not null,
	change_time	timestamp	not null,
	primary key(id)
);
create unique index system_status_system_id on system_status(system_id);

create table system_status_lines (
	system_status_id	integer		not null references system_status(id),
	line_no			integer		not null,
	entry_type		varchar(255)	not null,
	entry_name		varchar(255)	not null,
	entry_value		varchar(255)	not null,
	primary key(system_status_id, line_no)
);

create table system_messages (
	id		serial,
	send_time	timestamp	not null,
	system_id	integer		not null references systems(id),
	service_id	integer		not null references services(id),
	status_id	integer		not null references status(id),
	recieve_time	timestamp	not null,
	expire		integer		not null,
	str		text		not null,
	primary key(id)
);
create unique index system_mesages_index1 on system_messages(send_time, system_id, service_id, status_id);

create table system_message_lines (
	system_message_id	integer		not null references system_messages(id),
	line_no			integer		not null,
	entry_type		varchar(255)	not null,
	entry_name		varchar(255)	not null,
	entry_value		varchar(255)	not null,
	primary key(system_message_id, line_no)
);
