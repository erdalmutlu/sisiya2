Sequel.migration do
  change do
  	
  	create_table(:languages) do
  		String	:str,null: false
  		String	:code,null: false
  		String	:charset,null:false
  		Integer :id,primary_key: true
  	end
  
  	create_table(:labels) do
			primary_key	:id
  		Integer	:language_id,null: false
  		String	:label,null: false
  		Text		:str,null: false
  		index   [:language_id,:label],unique: true
  	end
 
 		create_table(:system_types) do
 			primary_key	:id
 			String	:str,null:false
 		end
 
 		create_table(:services) do
 			Integer :id,primary_key: true,null:false
 			String	:label,null:false
 		end 	
 	
  	create_table(:status) do
 			Integer :id,primary_key: true,null:false
 			String	:label,null:false
 		end
 
  	create_table(:properties) do
 			primary_key   :id,null:false
 			String	:code,null:false
 			String	:label,null:false
 			Text	:value,null:false
 		end
 
   	create_table(:alert_types) do
 			primary_key   :id,null:false
 			String	:code,null:false
 			String	:label,null:false
 			Text	:value,null:false
 		end
 
   	create_table(:locations) do
 			primary_key   :id,null:false
 			Integer	:sort_id,null:false
 			String	:label,null:false
 			Float		:latitude
 			Float		:longitude
 		end	

   	create_table(:systems) do
 			primary_key   :id,null:false
 			Boolean	:enabled,default: true
 			Integer	:system_type_id,null: false
 			Integer	:location_id,null: false
 			String	:hostname,null: false
 			Boolean	:is_critical,default: true
 			String	:ip,null: false
 			String	:mac,null: false
 		end
 		
 		create_table(:system_alerts) do
 			primary_key   :id,null:false
 			Integer	:user_id,null:false
 			Integer	:system_id,null:false
 			Integer	:alert_type_id,null:false
 			Integer	:status_id,null:false
 			Integer	:frequency,null:false
 			Boolean	:enabled,default: true
 			DateTime :last_alert_time
 			index   [:user_id,:system_id,:alert_type_id,:status_id],unique: true
 		end
 
  	create_table(:system_service_alerts) do
 			primary_key   :id,null:false
 			Integer	:user_id,null:false
 			Integer	:system_id,null:false
 			Integer	:service_id,null:false
 			Integer	:alert_type_id,null:false
 			Integer	:status_id,null:false
 			Integer	:frequency,null:false
 			Boolean	:enabled,default: true
 			DateTime :last_alert_time
 			index   [:user_id,:system_id,:service_id,:alert_type_id,:status_id],unique: true
 		end
 			
 		create_table(:system_status) do
 			primary_key   :id,null:false
 			Integer				:system_id,null: false
 			Integer				:status_id,null: false
 	    DateTime      :update_time
      DateTime      :change_time
 		end

 		create_table(:scanned_systems) do
 			primary_key   :id,null:false
 			Integer				:user_id,null: false
 			Integer				:system_type_id,null: false
			String				:hostname,null: false
			String				:fullhostname,null: false
			String				:ip,null: false
			String				:mac,null: false
			index   [:user_id,:hostname],unique: true
 		end
 
  	create_table(:system_service_status) do
 			primary_key   :id,null:false
 			Integer				:system_id,null: false
 			Integer				:service_id,null: false
 			Integer				:status_id,null: false
 	    DateTime      :update_time
      DateTime      :change_time
      Integer				:expire,null: false
      Text					:str,null: false
 		end
 	
 		create_table(:system_service_status_lines) do
 			primary_key   :id,null:false
 			Integer	:system_service_status_id,null: false
 			Integer	:line_no,null: false
 			String	:entry_type,null: false
 			String	:entry_name,null: false
 			String	:entry_value,null: false
 			index   [:system_service_status_id,:line_no],unique: true
 		end
 
   	create_table(:system_status_lines) do
 			primary_key   :id,null:false
 			Integer				:system_status_id,null: false
 			Integer				:line_no,null: false
 	 		String				:entry_type,null: false
 			String				:entry_name,null: false
 			String				:entry_value,null: false
			index   [:system_status_id,:line_no],unique: true
 		end
 	
 		create_table(:system_messages) do
 			primary_key   :id,null:false
 			Integer				:system_id,null: false
 			Integer				:service_id,null: false
 			Integer				:status_id,null: false
 			Integer				:expire,null: false
 			Text					:str,null: false
 			DateTime			:receive_time,null: false
 			DateTime      :send_time,null: false
 		end

   	create_table(:system_message_lines) do
 			primary_key   :id,null:false
 			Integer				:system_message_id,null: false
 			Integer				:line_no,null: false
 	 		String				:entry_type,null: false
 			String				:entry_name,null: false
 			String				:entry_value,null: false
			index   [:system_message_id,:line_no],unique: true
 		end 	
 	
    create_table(:users) do
      primary_key   :id
      String        :username, :null=>false
      String        :name, :null=>false
      String        :surname, :null=>false
      String        :email, :null=>false
      String        :password_hash, :null=>false	#Password_hash
      String        :temp_token
      Integer       :language_id
      String        :last_login_ip 
      String        :current_login_ip 
      Integer       :failed_login_count
      Integer       :login_count
      DateTime      :last_login_at
      DateTime      :current_login_at
      DateTime      :last_request_at     
      Integer       :role_id
      DateTime      :create_date
      DateTime      :temp_token_created_at
      index					:username
    end
   	
    create_table(:uploads) do
      primary_key   :id
      String        :name
      String        :type
      Integer       :size
      Text          :blob				#Base64 text here
      String        :model_name
      Integer       :model_id
      index         [:model_name,:model_id]
    end
   
  end
end
