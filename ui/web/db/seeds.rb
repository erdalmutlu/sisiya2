require ::File.expand_path('../../ruby/init.rb',  __FILE__)
require 'yaml'
DB[:users].insert(name: "admin",username: "admin",surname: "admin",email: "admin@example.com",password_hash: "jQ5rLevzXz3NpWbIGlqd3k4r3ZfJrEdma4maaP8b6wg=$xP/LXRa+mHFjNDJuH9FUlSzvCE3AlRynnZcJ2VCQkpg=")
DB[:users].insert(name: "admin2",username: "admin2",surname: "admin2",email: "admin2@example.com",password_hash: "jQ5rLevzXz3NpWbIGlqd3k4r3ZfJrEdma4maaP8b6wg=$xP/LXRa+mHFjNDJuH9FUlSzvCE3AlRynnZcJ2VCQkpg=")
#Default Languages
DB[:languages].insert(id: 1,str: "English",code: "EN",charset: "UTF-8")
DB[:languages].insert(id: 2,str: "Türkçe",code: "TR",charset: "UTF-8")
#System types
File.read("#{APP_ROOT}/db/seed_data/sisiya_system_types.csv").each_line do |line|
	#DB[:system_types].insert(id: line.split("|")[0].to_i,label: line.split("|")[1].gsub("\n",""))
	DB[:system_types].insert(str: line.gsub("\n",""))
end
#Services
File.read("#{APP_ROOT}/db/seed_data/services.csv").each_line do |line|
	DB[:services].insert(id: line.split("|")[0].to_i ,label: line.split("|")[1].gsub("\n",""))
end
#Default status
File.read("#{APP_ROOT}/db/seed_data/statuses.csv").each_line do |line|
	DB[:status].insert(id: line.split("|")[0].to_i ,label: line.split("|")[1].gsub("\n",""))
end
#Default Label
en = YAML.load_file("#{APP_ROOT}/i18n/en.yml");
tr = YAML.load_file("#{APP_ROOT}/i18n/tr.yml");
en["en"].each do |l|
	DB[:labels].insert(language_id: 1,label: l[0],str: l[1])
end
tr["tr"].each do |l|
	DB[:labels].insert(language_id: 2,label: l[0],str: l[1])
end