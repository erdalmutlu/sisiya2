/*
    Copyright (C) Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _trim_hpp_
#define _trim_hpp_

#include<string>

//! Trim whitespaces / tabs from left (copying)
std::string ltrim_copy(const std::string &s);

//! Trim whitespaces / tabs from right (copying)
std::string rtrim_copy(const std::string &s); 

//! Trim whitespaces / tabs from both sides (copying)
std::string trim_copy(const std::string &s); 

//! Trim whitespaces / tabs from both sides (in place)
void trim(std::string &s); 

#endif
