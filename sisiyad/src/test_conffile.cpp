#include<iostream>
#include"ConfFile.hpp"

using namespace std;

void showConfigs(const ConfFile &confFile)
{
	cout << "StartServers		: [" << confFile.getInt("StartServers") << "]" << endl;
	cout << "MaxClients		: [" << confFile.getInt("MaxClients") << "]" << endl;
	cout << "MinSpareThreads		: [" << confFile.getInt("MinSpareThreads") << "]" << endl;
	cout << "MaxSpareThreads		: [" << confFile.getInt("MaxSpareThreads") << "]" << endl;
	cout << "ReadTimeout		: [" << confFile.getInt("ReadTimeout") << "]" << endl;
	cout << "PID_FILE		: [" << confFile.getString("PID_FILE") << "]" << endl;
	cout << "LOGLEVEL		: [" << confFile.getInt("LOGLEVEL") << "]" << endl;
	cout << "IP			: [" << confFile.getString("IP") << "]" << endl;
	cout << "PORT			: [" << confFile.getInt("PORT") << "]" << endl;
	cout << "SSL_PORT		: [" << confFile.getInt("SSL_PORT") << "]" << endl;
	cout << "DB_SERVER		: [" << confFile.getString("DB_SERVER") << "]" << endl;
	cout << "DB_TYPE			: [" << confFile.getString("DB_TYPE") << "]" << endl;
	cout << "DB_NAME			: [" << confFile.getString("DB_NAME") << "]" << endl;
	cout << "DB_USER			: [" << confFile.getString("DB_USER") << "]" << endl;
	cout << "DB_PASSWORD		: [" << confFile.getString("DB_PASSWORD") << "]" << endl;
}

void setDefaults(ConfFile &confFile)
{
	confFile.setDefault("StartServers", 5);
	confFile.setDefault("MaxClients", 600);
	confFile.setDefault("MinSpareThreads", 10);
	confFile.setDefault("MaxSpareThreads", 30);
	confFile.setDefault("ReadTimeout", 10);
	confFile.setDefault("PID_FILE", "/var/run/sisiyad.pid");
	confFile.setDefault("LOGLEVEL", 0);
	confFile.setDefault("SEMKEY", 11);
	confFile.setDefault("IP", "any");
	confFile.setDefault("PORT", 8888);
	confFile.setDefault("SSL_PORT", 8889);
	confFile.setDefault("DB_SERVER", "localhost");
	confFile.setDefault("DB_TYPE","PostgreSQL");
	//confFile.setDefault("DB_TYPE", "MySQL");
	confFile.setDefault("DB_NAME", "sisiya");
	confFile.setDefault("DB_USER", "sisiyauser");
	confFile.setDefault("DB_PASSWORD", "sisiyauser1");
	if (confFile.getString("DB_TYPE") == "PostgreSQL")
		confFile.setDefault("DB_PORT", 5432);
	else if (confFile.getString("DB_TYPE") == "MySQL")
		confFile.setDefault("DB_PORT", 3306);
	else
		confFile.setDefault("DB_PORT", 0);
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		cerr << "Usage : " << argv[0] << " string" << endl;
		return 1;
	}
	ConfFile confFile(argv[1]);
	//string fileName(argv[1]);
	//ConfFile confFile(fileName);
	setDefaults(confFile);
	showConfigs(confFile);
	return 0;
}
