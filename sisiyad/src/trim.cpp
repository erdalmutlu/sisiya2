/*
    Copyright (C) Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include<string>
#include"trim.hpp"

std::string ltrim_copy(const std::string &s) 
{
	auto length = s.length();
	if(length == 0)
		return s;
	auto b = s.find_first_not_of(" \t");
	if(b == std::string::npos)
		return "";	// no non-space chars

	return s.substr(b, length - b + 1);
}

std::string rtrim_copy(const std::string &s) 
{
	if(s.length() == 0)
		return s;
	auto pos = s.find_last_not_of(" \t");
	if(pos == std::string::npos)
		return s;

	return s.substr(0, pos + 1);
}

std::string trim_copy(const std::string &s) 
{
	if(s.length() == 0)
		return s;
	auto b = s.find_first_not_of(" \t");
	if(b == std::string::npos)
		return "";	// no non-space chars
	auto e = s.find_last_not_of(" \t");

	return s.substr(b, e - b + 1);
}

void trim(std::string &s) 
{
	if(s.length() == 0)
		return;
	auto b = s.find_first_not_of(" \t");
	if(b == std::string::npos)
		return;	// no non-space chars
	auto e = s.find_last_not_of(" \t");

	s = s.substr(b, e - b + 1);
}

/*
std::string trim_copy(std::string &s) 
{
	return rtrim_copy( ltrim_copy(s) );
}
*/
