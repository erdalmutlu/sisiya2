/*
    Copyright (C) Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<vector>
#include<map>
#include"trim.hpp"

using namespace std;

	namespace { 
		const char comment_char = '#';
		const char delimiter_char = '=';
	};

bool extractKeyValue(const std::string &str, std::string &key, std::string &value)
{
	// empty key and value
	key.erase();
	value.erase();


	// remove the rest starting from #
	auto pos = str.find(comment_char);
	std::string s;
	if (pos == std::string::npos)
		s = str;
	else
		s = str.substr(0, pos);

	// check for key/value delimiter
	pos = s.find(delimiter_char);
	if (pos == std::string::npos)
		return false;

	// extract key and value
	key = trim_copy(s.substr(0, pos));
	value = trim_copy(s.substr(pos + 1));
	return true;
}

void extractKeyValues(std::ifstream &file)
{
	std::string line;
	std::string key;
	std::string value;

	while(std::getline(file, line)) {
		cout << "extractKeyValues: line=[" << line << "]" << endl;
		if (extractKeyValue(line, key, value))
			cout << "extractKeyValues: key=[" << key << "] value=[" << value << "]" << endl;
	}
}

void extractKeyValue_test()
{
	string input("DEBUG= 0\nMerhaba dünya\nUSER_NAME = sisiyauser\nPORT=8889\nDB_TYPE = MySQL");
	istringstream iss(input);

	map<string, string> conf_options;

	cout << "input=[" << input << "]" << endl;
	string token, key, value;
	while (std::getline(iss, token, '\n')) {
		if (extractKeyValue(token, key, value) == false)
			cout << "str=[" << token << "] does not have key=value" << endl;
		else
			cout << "key=[" << key << "] value=[" << value << "]" << endl;
	/*	auto pos = token.find('=');
		if (pos == string::npos) {
			cout << "str=[" << token << "] does not have key=value" << endl;
		} else {
			key = token.substr(0, pos);
			value = token.substr(pos + 1);
			cout << "key=[" << key << "] value=[" << value << "]" << endl;
		}
	*/
	}
}

int main(int argc, char *argv[])
{
	std::ifstream file;

	if (argc != 2) {
		cerr << "Usage: " << argv[0] << " conf_file" << endl;
		return 1;
	}
	
	file.open(argv[1], std::ifstream::in);
	if (!file.good()) {
		cerr << argv[0] << ": Could not open file: " << argv[1] << endl;
		return 1;
	}
	extractKeyValues(file);
	file.close();
	
	return 0;
}
