/*
    Copyright (C) 2005  Erdal Mutlu

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
*/

#include<iostream>
#include<fstream>
#include<sstream>
#include<list>
#include<map>

#include"ConfFile.hpp"
#include"trim.hpp"

namespace { 
	const char comment_char = '#';
	const char delimiter_char = '=';
};

/*!
The default constructor.
*/
ConfFile::ConfFile()
	: options{}
{
}

/*!
Constructor with a parameter: fileName
*/
ConfFile::ConfFile(const char *fileName)
	: options{} 
{
	load(fileName);
}

/*!
Constructor with a parameter: fileName
*/
ConfFile::ConfFile(const string &fileName)
	: options{} 
{
	load(fileName);
}

ConfFile::~ConfFile()
{
}

bool ConfFile::extractKeyValue(const std::string &str, string &key, string &value)
{
	// empty key and value
	key.erase();
	value.erase();


	// remove the rest starting from #
	auto pos = str.find(comment_char);
	std::string s;
	if (pos == std::string::npos)
		s = str;
	else
		s = str.substr(0, pos);

	// check for key/value delimiter
	pos = s.find(delimiter_char);
	if (pos == std::string::npos)
		return false;

	// extract key and value
	key = trim_copy(s.substr(0, pos));
	value = trim_copy(s.substr(pos + 1));
	return true;
}

/*!
Extracts all key/value pairs from the file. The format is key=value.
*/
void ConfFile::extractKeyValues(std::ifstream &file)
{
	std::string line, key, value;

	while(std::getline(file, line)) {
		if (extractKeyValue(line, key, value)) {
			auto itr = options.find(key);
			if (itr == options.end())
				options.insert(make_pair(key, value));
			else
				(*itr).second = value;
		}
	}

	/*
	cout << "ConfFile::extractKeyValues: Now printing key/value from the map object:" << endl;
	for(map<string,string>::iterator i=options.begin();i!=options.end();i++) {
		cout << "ConfFile::extractKeyValues: key=[" << (*i).first << "] value=[" << (*i).second << "]" << endl;
	}
	*/
}

double ConfFile::getDouble(const char *key) const
{
	return getDouble(string(key));
}

double ConfFile::getDouble(const string key) const
{
	return atof(getKeyValue(key).c_str());
}

float ConfFile::getFloat(const char *key) const
{
	return getFloat(string(key));
}

float ConfFile::getFloat(const string key) const
{
	return float(atof(getKeyValue(key).c_str()));
}

int ConfFile::getInt(const char *key) const
{
	return getInt(string(key));
}

int ConfFile::getInt(const string key) const
{
	string s = getKeyValue(key);
	if (s.size() == 0)
		return ConfFile::intNotFound;

	istringstream isstr(s);
	int i;
	isstr >> i;
	return i;
}

/*!
Search for the key and return its value as string. If not found return an empty string.
*/
string ConfFile::getKeyValue(const string key) const
{
	if (options.count(key) == 1) {
		//map<string, string>::iterator itr = options.find(key);
		auto itr = options.find(key);
		return (*itr).second;
	} else
		return string();
}

const string ConfFile::getString(const char *key) const
{
	return getString(string(key));
}

const string ConfFile::getString(const string key) const
{
	return string(getKeyValue(key));
}

/*!
Check if the line a comment or empty is. Comment char is '#'.
*/
bool ConfFile::isLineCommentOrEmpty(const char *line, const char ch) const
{
	if (line[0] == '\0')
		return true;	// empty line

	// check if the line is a configuration line or if it is only an empty or comment line
	// find the first char which is not ' ','\t' or ch
	for (int i = 0; line[i] != '\0'; i++) {
		if (line[i] == ' ' || line[i] == '\t')
			continue;
		else if (line[i] == ch)
			return true;
		else
			return false;
	}
	return false;
}


void ConfFile::setDefault(const char *key, int value)
{
	ostringstream osstr;
	osstr << value << ends;
	setDefault(string(key), osstr.str());
}

void ConfFile::setDefault(string key, int value)
{
	ostringstream osstr;
	osstr << value << ends;
	setDefault(key, osstr.str());
}

void ConfFile::setDefault(const char *key, const char *value)
{
	setDefault(string(key), string(value));
}

void ConfFile::setDefault(const string &key, const string &value)
{
	if (options.count(key) == 0) {
		options.insert(make_pair(key, value));
	} else {
		map < string, string >::iterator i = options.find(key);
		(*i).second = value;
	}
}

bool ConfFile::load(const char *fileName)
{
	return(load(string(fileName)));
}

bool ConfFile::load(const std::string &fileName)
{

	std::ifstream file;

	file.open(fileName, std::ifstream::in);
	if (!file.good()) { // I should throw an IO exception here
		cerr << "ConfFile::setFile: Could not open file : " << fileName << endl;
		return false;
	}
	extractKeyValues(file);
	file.close();
	return true;
}
